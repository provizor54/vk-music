package gq.musicapp.portable.musicapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;

import io.realm.Realm;

import static gq.musicapp.portable.musicapp.AppConst.APP_PREF;
import static gq.musicapp.portable.musicapp.AppConst.LOCAL_STORE_URL;

public class Application extends android.app.Application {

    private File folder;
    private SharedPreferences mSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this, getResources().getString(R.string.admob_app_id));
        Realm.init(this);

        mSharedPreferences = getSharedPreferences(APP_PREF, MODE_PRIVATE);

        if (mSharedPreferences.contains("hasUpdate")) {
            if (mSharedPreferences.getBoolean("hasUpdate", false) == true) {
                String url = mSharedPreferences.getString("url", "");
                String googlePlayUrl = mSharedPreferences.getString("googlePlayUrl", "");

                startActivity(new Intent(Application.this, UpdateActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .putExtra("url", url)
                        .putExtra("googlePlayUrl", googlePlayUrl));
            }
        }

        if (!mSharedPreferences.contains("access_token")) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        }

        folder = new File(Environment.getExternalStorageDirectory() + LOCAL_STORE_URL);

        if (!folder.exists()) {
            folder.mkdirs();
        }
    }
}