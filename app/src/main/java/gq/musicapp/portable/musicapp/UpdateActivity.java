package gq.musicapp.portable.musicapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class UpdateActivity extends AppCompatActivity {

    private Button mInstall;
    /*private DatabaseReference mRef;*/
    private String mUrl;
    private String mGooglePlayUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        Intent intent = getIntent();

        mUrl = intent.getStringExtra("url");
        mGooglePlayUrl = intent.getStringExtra("googlePlayUrl");

        mInstall = (Button) findViewById(R.id.install_button);
        mInstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mGooglePlayUrl)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl)));
                }
            }
        });
    }
}
