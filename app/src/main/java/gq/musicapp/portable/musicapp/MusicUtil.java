package gq.musicapp.portable.musicapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.clockbyte.admobadapter.expressads.AdmobExpressRecyclerAdapterWrapper;
import com.google.android.gms.ads.AdRequest;

import static gq.musicapp.portable.musicapp.AppConst.APP_PREF;

/**
 * Created by Portable on 9/23/2015.
 */
public class MusicUtil {
    public static String convertMilliseconds(int milliseconds) {
        int hours;
        int minutes;
        int seconds;

        String result;

        if (milliseconds >= 3600000) {
            hours = (int) (milliseconds / 1000.0 / 60.0 / 60.0);
            minutes = (int) (milliseconds / 1000.0 / 60.0 % 60.0);
            seconds = (int) (milliseconds / 1000.0 % 60.0);

            result = String.format("%d:%02d:%02d", hours, minutes, seconds);
        } else {
            minutes = (int) (milliseconds / 1000.0 / 60.0);
            seconds = (int) (milliseconds / 1000.0 % 60.0);

            result = String.format("%02d:%02d", minutes, seconds);
        }

        return result;
    }

    public static String convertSeconds(int seconds) {
        int hours;
        int minutes;
        int sec;

        String result;

        if (seconds >= 3600) {
            hours = (int) (seconds / 60.0 / 60.0);
            minutes = (int) (seconds / 60.0 % 60.0);
            sec = (int) (seconds % 60.0);

            result = String.format("%d:%02d:%02d", hours, minutes, sec);
        } else {
            minutes = (int) (seconds / 60.0);
            sec = (int) (seconds % 60.0);

            result = String.format("%02d:%02d", minutes, sec);
        }

        return result;
    }

    public static AdRequest getAdRequest() {
        // Create an ad request. Check your logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
        return new AdRequest.Builder()
                .addTestDevice("B9E4122C7C09DAA63C0659B648534D27")
                .addTestDevice("65228D7D1007FEF665A69F22CDAC3965")
                .addTestDevice("A6D2B392FF2AE44ADDF337854D850212")
                .build();
    }

    public static AdmobExpressRecyclerAdapterWrapper getAdapterWrapper(Context context) {
        AdmobExpressRecyclerAdapterWrapper adapterWrapper = new AdmobExpressRecyclerAdapterWrapper(context,
                context.getResources().getStringArray(R.array.test_device_id)) {

        };

        adapterWrapper.setLimitOfAds(1000);
        adapterWrapper.setFirstAdIndex(2);
        adapterWrapper.setNoOfDataBetweenAds(10);

        return adapterWrapper;
    }

    public static void showRateDialog(final Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit();
        editor.putInt("rate_dialog_showed", 1);
        editor.apply();

        final String url = "https://play.google.com/store/apps/details?id=" + context.getPackageName();
        final String googlePlayUrl = "market://details?id=" + context.getPackageName();

        new AlertDialog.Builder(context)
                .setTitle("Привет")
                .setMessage("Зайди в Google Play и поставь пожалуйста оценку 5 приложению, ты так очень мне поможешь ;-)")
                .setCancelable(false)
                .setPositiveButton("ХОРОШО", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(googlePlayUrl)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        }

                        Toast.makeText(context, "Уии, спасибо, люблю тебя <3", Toast.LENGTH_SHORT).show();
                    }
                }).show();
    }
}
