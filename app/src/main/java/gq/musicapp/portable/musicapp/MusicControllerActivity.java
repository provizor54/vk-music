package gq.musicapp.portable.musicapp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.devbrackets.android.exomedia.EMAudioPlayer;

public class MusicControllerActivity extends AppCompatActivity implements MusicService.ServiceCallbacksMusicController, SeekBar.OnSeekBarChangeListener {

    private MusicService mMusicService;
    private ServiceConnection mServiceConnection;
    private boolean mBound = false;
    private ImageView mNextMusic;
    private ImageView mPrevMusic;
    private ImageView mPlayMusic;
    private ImageView mRepeatMusic;
    private ImageView mShuffleMusic;
    private TextView mTitle;
    private TextView mArtist;
    private TextView mCurrentSecond;
    private TextView mEndSecond;
    private SeekBar mSeekbarMusic;
    private EMAudioPlayer mMediaPlayer;
    private Handler handler;
    private Runnable updateTime;
    private ImageView mBroadcastMusic;
    private ImageView mAddToPlaylist;
    private ImageView mCurrentPlaylist;
    private ImageView mMusicImage;
    private ImageView mDownloadMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_controller);

        handler = new Handler();
        updateTime = new Runnable() {
            String str;

            @Override
            public void run() {
                str = MusicUtil.convertMilliseconds(mMediaPlayer.getCurrentPosition());

                mCurrentSecond.setText(str);
                mSeekbarMusic.setProgress(mMediaPlayer.getCurrentPosition());

                handler.postDelayed(this, 250);
            }
        };

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMusicService = ((MusicService.MusicBinder) service).getService();
                mBound = true;
                mMusicService.setServiceCallbacksMusicController(MusicControllerActivity.this);

                mMediaPlayer = mMusicService.getMediaPlayer();

                if (mMusicService.isPlaying()) {
                    mPlayMusic.setImageResource(R.drawable.ic_pause_black_24dp);
                } else {
                    mPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                }

                if (mMusicService.isLooping()) {
                    mRepeatMusic.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
                } else {
                    mRepeatMusic.setColorFilter(Color.BLACK);
                }

                if (mMusicService.isShuffling()) {
                    mShuffleMusic.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
                } else {
                    mShuffleMusic.setColorFilter(Color.BLACK);
                }

                if (mMusicService.isInMyPlaylist()) {
                    mAddToPlaylist.setImageResource(R.drawable.ic_playlist_add_check_black_24dp);
                    mAddToPlaylist.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
                }

                updateUIForMusicController();

                handler.post(updateTime);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        bindService(new Intent(this, MusicService.class), mServiceConnection, BIND_AUTO_CREATE);

        mNextMusic = (ImageView) findViewById(R.id.nextMusic);
        mPrevMusic = (ImageView) findViewById(R.id.prevMusic);
        mPlayMusic = (ImageView) findViewById(R.id.playMusic);
        mRepeatMusic = (ImageView) findViewById(R.id.repeatMusic);
        mShuffleMusic = (ImageView) findViewById(R.id.shuffleMusic);
        mTitle = (TextView) findViewById(R.id.title);
        mArtist = (TextView) findViewById(R.id.artist);
        mCurrentSecond = (TextView) findViewById(R.id.current_second);
        mEndSecond = (TextView) findViewById(R.id.end_second);
        mSeekbarMusic = (SeekBar) findViewById(R.id.seekBar);
        mSeekbarMusic.setOnSeekBarChangeListener(this);
        mBroadcastMusic = (ImageView) findViewById(R.id.broadcast_music);
        mAddToPlaylist = (ImageView) findViewById(R.id.add_to_playlist);
        mCurrentPlaylist = (ImageView) findViewById(R.id.current_playlist);
        mMusicImage = (ImageView) findViewById(R.id.music_image);
        mDownloadMusic = (ImageView) findViewById(R.id.music_download);

        mDownloadMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.downloadMusic(mMusicService.getCurrentAudio());
            }
        });

        mCurrentPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MusicControllerActivity.this, CurrentPlayListActivity.class));
            }
        });

        mAddToPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.addToPlaylist();
            }
        });

        mBroadcastMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.broadcastMusic();
            }
        });

        mNextMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.nextMusic();
            }
        });

        mPrevMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.previousMusic();
            }
        });

        mPlayMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int musicState = mMusicService.pause();

                if (musicState == MusicService.MUSIC_PLAY) {
                    mPlayMusic.setImageResource(R.drawable.ic_pause_black_24dp);
                } else {
                    mPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                }
            }
        });

        mRepeatMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMusicService.isLooping()) {
                    mRepeatMusic.setColorFilter(Color.BLACK);
                    mMusicService.setLooping(false);
                } else {
                    mRepeatMusic.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
                    mMusicService.setLooping(true);
                }
            }
        });

        mShuffleMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMusicService.isShuffling()) {
                    mShuffleMusic.setColorFilter(Color.BLACK);
                    mMusicService.setShuffle(false);
                } else {
                    mShuffleMusic.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
                    mMusicService.setShuffle(true);
                }
            }
        });
    }

    @Override
    public void updateUIForMusicController() {
        if (mMusicService != null) {
            mTitle.setText(mMusicService.getCurrentMusicTitle());
            mArtist.setText(mMusicService.getCurrentMusicArtist());
            mEndSecond.setText(MusicUtil.convertMilliseconds(mMusicService.getDuration()));
            mSeekbarMusic.setMax(mMusicService.getDuration());
            //mMusicService.updateUIMusicImage();
            updateUIMusicImage(mMusicService.getmMusicImage());
            updateUIinMyPlayList(mMusicService.isInMyPlaylist());
        }
    }

    @Override
    public void updateUIBroadcast() {
        if (mMusicService != null) {
            if (mMusicService.isBroadcast()) {
                mBroadcastMusic.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
            } else {
                mBroadcastMusic.setColorFilter(Color.BLACK);
            }
        }
    }

    @Override
    public void updateUIaddToPlaylist(int value) {
        if (value == MusicService.ADD_TO_PLAYLIST) {
            mAddToPlaylist.setImageResource(R.drawable.ic_playlist_add_check_black_24dp);
            mAddToPlaylist.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
        } else if (value == MusicService.REMOVE_FROM_PLAYLIST) {
            mAddToPlaylist.setImageResource(R.drawable.ic_playlist_add_black_24dp);
            mAddToPlaylist.setColorFilter(Color.BLACK);
        }
    }

    @Override
    public void updateUIMusicImage(Bitmap musicImage) {
        if (musicImage != null) {
            mMusicImage.setImageBitmap(musicImage);
        } else {
            mMusicImage.setImageBitmap(null);
        }
    }

    @Override
    public void updateUIDownloadMusic(boolean enabled) {
        mDownloadMusic.setEnabled(enabled);
    }

    @Override
    public void updateUIPlayToggle(boolean isPlay) {
        if (isPlay) {
            mPlayMusic.setImageResource(R.drawable.ic_pause_black_24dp);
        } else {
            mPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        }
    }

    @Override
    public void updateUIinMyPlayList(boolean isInMyPlayList) {
        if (isInMyPlayList) {
            mAddToPlaylist.setImageResource(R.drawable.ic_playlist_add_check_black_24dp);
            mAddToPlaylist.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, R.color.colorElement));
        } else {
            mAddToPlaylist.setImageResource(R.drawable.ic_playlist_add_black_24dp);
            mAddToPlaylist.setColorFilter(ContextCompat.getColor(MusicControllerActivity.this, android.R.color.black));
        }
    }

    @Override
    protected void onDestroy() {
        if (handler != null) {
            handler.removeCallbacks(updateTime);
        }

        //mMusicService.setServiceCallbacksMusicController(null);

        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }

        super.onDestroy();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        Log.d("myLogs", String.valueOf(seekBar.getProgress()));
        mMusicService.seekTo(seekBar.getProgress());
    }
}
