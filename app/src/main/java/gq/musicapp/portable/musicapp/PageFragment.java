package gq.musicapp.portable.musicapp;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.clockbyte.admobadapter.expressads.AdmobExpressRecyclerAdapterWrapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import wseemann.media.FFmpegMediaMetadataRetriever;

import static android.content.Context.MODE_PRIVATE;
import static gq.musicapp.portable.musicapp.AppConst.BASE_URL;
import static gq.musicapp.portable.musicapp.AppConst.SECOND_PART_URL;

/**
 * Created by Portable on 05.10.2016.
 */

public class PageFragment extends Fragment {
    public static final String LOG_TAG = "myTag";
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final int SAVED_MUSIC = 0;
    public static final int USER_MUSIC = 1;
    public static final int POPULAR_MUSIC = 2;
    public static final int RECOMMENDATION_MUSIC = 3;
    public static final int REQUEST_CODE_READ_EXT = 4;
    public static final int MAX_ITEM_PER_REQUEST = 100;

    private int mPage;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<Music> audios;
    private BaseAttacher mBaseAttacher;
    private boolean isLoading = false;
    private ProgressBar mLoadingProgressBar;
    private String mAccessToken;
    private String mUser_Id;
    private SharedPreferences mSharedPreferences;
    private OkHttpClient mOkHttpClient;
    private Handler mHandler;
    private FFmpegMediaMetadataRetriever mRetriever;
    private AdmobExpressRecyclerAdapterWrapper mAdapterWrapper;

    public static PageFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);

        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPage = getArguments().getInt(ARG_PAGE);
        mSharedPreferences = getContext().getSharedPreferences(AppConst.APP_PREF, MODE_PRIVATE);
        mAccessToken = mSharedPreferences.getString("access_token", "");
        mUser_Id = mSharedPreferences.getString("user_id", "");
        mOkHttpClient = new OkHttpClient();
        mHandler = new Handler();
        mAdapterWrapper = MusicUtil.getAdapterWrapper(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;

        Request request;

        switch (mPage) {
            case SAVED_MUSIC:
                view = getMusic(inflater, container);
                break;
            case USER_MUSIC:
                request = new Request.Builder()
                        .url(BASE_URL + "audio.get?" + SECOND_PART_URL + mAccessToken
                                + "&owner_id=" + mUser_Id + "&count=200")
                        .build();
                view = vkMusicRequest(inflater, container, request);
                break;
            case RECOMMENDATION_MUSIC:
                request = new Request.Builder()
                        .url(BASE_URL + "audio.getRecommendations?" + SECOND_PART_URL + mAccessToken
                                + "&user_id=" + mUser_Id + "&count=200")
                        .build();
                view = vkMusicRequest(inflater, container, request);
                break;
            case POPULAR_MUSIC:
                request = new Request.Builder()
                        .url(BASE_URL + "audio.getPopular?" + SECOND_PART_URL + mAccessToken + "&count=200")
                        .build();
                view = vkMusicRequest(inflater, container, request);
                break;
        }

        return view;
    }

    public View getMusic(LayoutInflater inflater, ViewGroup container) {
        SavedMusicAdapter savedMusicAdapter;
        View view;
        Realm realm = Realm.getDefaultInstance();

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            askForPermission();

            return null;
        }

        final RealmResults<Music> audios = realm.where(Music.class).findAll();

        view = inflater.inflate(R.layout.fragment_page, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        savedMusicAdapter = new SavedMusicAdapter(audios, getContext());
        mRecyclerView.setAdapter(savedMusicAdapter);

        /*mRecyclerView.addOnItemTouchListener(new RecyclerClickListener(getActivity(), new RecyclerClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ((MainActivity) getActivity()).playMusic(audios, position);
            }
        }));*/

        return view;
    }

/*    public View getMusic(LayoutInflater inflater, ViewGroup container) {
        String artist;
        String title;
        String url;
        int duration;
        View view;
        Music audio;
        audios = new ArrayList<>();
        mRetriever = new FFmpegMediaMetadataRetriever();

        File file = new File(Environment.getExternalStorageDirectory() + LOCAL_STORE_URL);


        File[] files = file.listFiles();

        for (File f : files) {
            mRetriever.setDataSource(f.getAbsolutePath().toString());
            artist = mRetriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
            title = mRetriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_TITLE);
            url = f.getAbsolutePath().toString();
            duration = Integer.parseInt(mRetriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION));

            audio = new Music(artist, title, duration / 1000, "", "", url);
            audio.setUrl(f.getPath());

            audios.add(audio);
        }

        view = inflater.inflate(R.layout.fragment_page, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerAdapter(audios);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerClickListener(getActivity(), new RecyclerClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ((MainActivity) getActivity()).playMusic(audios, position);
            }
        }));

        mRetriever.release();

        return view;
    }*/

    public View vkMusicRequest(final LayoutInflater inflater, final ViewGroup container, final Request request) {
        final View view = inflater.inflate(R.layout.fragment_page, container, false);
        mLoadingProgressBar = (ProgressBar) view.findViewById(R.id.loadingProgressBar);

        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // TODO: 26.12.2016

                mOkHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        final String result = response.body().string();

                        final JSONArray jsonArray;

                        try {
                            switch (mPage) {
                                case 1:
                                case 3:
                                    jsonArray = new JSONObject(result)
                                            .getJSONObject("response")
                                            .getJSONArray("items");
                                    break;
                                case 2:
                                    jsonArray = new JSONObject(result)
                                            .getJSONArray("response");
                                    break;
                                default:
                                    jsonArray = null;
                                    break;
                            }

                            Type listType = new TypeToken<ArrayList<Music>>() {
                            }.getType();
                            Gson gson = new Gson();
                            audios = gson.fromJson(jsonArray.toString(), listType);

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

                                    mLayoutManager = new LinearLayoutManager(getContext());
                                    mRecyclerView.setLayoutManager(mLayoutManager);
                                    mRecyclerView.setHasFixedSize(false);
                                    mAdapter = new RecyclerAdapter(audios, mPage, getContext());
                                    /*mAdapterWrapper.setAdapter(mAdapter);
                                    mRecyclerView.setAdapter(mAdapterWrapper);*/
                                    mRecyclerView.setAdapter(mAdapter);
                                    mBaseAttacher = Mugen.with(mRecyclerView, new MugenCallbacks() {
                                        @Override
                                        public void onLoadMore() {
                                            OkHttpClient innerOkHttp = new OkHttpClient();

                                            final Request innerRequest;

                                            switch (mPage) {
                                                case USER_MUSIC:
                                                    innerRequest = new Request.Builder()
                                                            .url(BASE_URL + "audio.get?" + SECOND_PART_URL + mAccessToken
                                                                    + "&owner_id=" + mUser_Id + "&offset=" + audios.size() + "&count=200")
                                                            .build();
                                                    break;
                                                case RECOMMENDATION_MUSIC:
                                                    innerRequest = new Request.Builder()
                                                            .url(BASE_URL + "audio.getRecommendations?" + SECOND_PART_URL + mAccessToken
                                                                    + "&user_id=" + mUser_Id + "&offset=" + audios.size() + "&count=200")
                                                            .build();
                                                    break;
                                                case POPULAR_MUSIC:
                                                    innerRequest = new Request.Builder()
                                                            .url(BASE_URL + "audio.getPopular?" + SECOND_PART_URL + mAccessToken + "&offset=" + audios.size() + "&count=200")
                                                            .build();
                                                    break;
                                                default:
                                                    innerRequest = null;
                                                    break;
                                            }

                                            mHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mLoadingProgressBar.setVisibility(View.VISIBLE);
                                                }
                                            });

                                            innerOkHttp.newCall(innerRequest).enqueue(new Callback() {
                                                @Override
                                                public void onFailure(Call call, IOException e) {

                                                }

                                                @Override
                                                public void onResponse(Call call, Response response) throws IOException {
                                                    final String innerResult = response.body().string();

                                                    JSONArray innerJsonArray = null;

                                                    try {
                                                        switch (mPage) {
                                                            case 1:
                                                            case 3:
                                                                innerJsonArray = new JSONObject(innerResult)
                                                                        .getJSONObject("response")
                                                                        .getJSONArray("items");
                                                                break;
                                                            case 2:
                                                                innerJsonArray = new JSONObject(innerResult)
                                                                        .getJSONArray("response");
                                                                break;
                                                        }

                                                        Type listType = new TypeToken<ArrayList<Music>>() {
                                                        }.getType();
                                                        Gson gson = new Gson();
                                                        List<Music> tempAudios = gson.fromJson(innerJsonArray.toString(), listType);

                                                        audios.addAll(tempAudios);

                                                        mHandler.post(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                mAdapter.notifyDataSetChanged();
                                                                mLoadingProgressBar.setVisibility(View.INVISIBLE);
                                                            }
                                                        });
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                        }

                                        @Override
                                        public boolean isLoading() {
                                            return isLoading;
                                        }

                                        @Override
                                        public boolean hasLoadedAllItems() {
                                            return false;
                                        }
                                    }).start();

                                    /*mRecyclerView.addOnItemTouchListener(new RecyclerClickListener(getActivity(), new RecyclerClickListener.ClickListener() {
                                        @Override
                                        public void onClick(View view, int position) {
                                            ((MainActivity) getActivity()).playMusic(audios, position);
                                        }
                                    }));*/
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        /*request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                audios = (VkAudioArray) response.parsedModel;

                mLoadingProgressBar = (ProgressBar) view.findViewById(R.id.loadingProgressBar);
                mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
                mRecyclerView.setHasFixedSize(true);

                mLayoutManager = new LinearLayoutManager(getContext());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new RecyclerAdapter(audios);
                mRecyclerView.setAdapter(mAdapter);
                mBaseAttacher = Mugen.with(mRecyclerView, new MugenCallbacks() {
                    @Override
                    public void onLoadMore() {
                        VKRequest request = null;

                        Log.d("myLogs", "onLoadMore");

                        switch (mPage) {
                            case SAVED_MUSIC:
                                break;
                            case USER_MUSIC:
                                request = VKApi.audio().get(VKParameters.from(VKApiConst.OFFSET, mAdapter.getItemCount(),
                                        VKApiConst.COUNT, MAX_ITEM_PER_REQUEST));
                                break;
                            case RECOMMENDATION_MUSIC:
                                request = VKApi.audio().getRecommendations(VKParameters.from(VKApiConst.OFFSET, mAdapter.getItemCount(),
                                        VKApiConst.COUNT, MAX_ITEM_PER_REQUEST));
                                break;
                            case POPULAR_MUSIC:
                                request = VKApi.audio().getPopular(VKParameters.from(VKApiConst.OFFSET, mAdapter.getItemCount(),
                                        VKApiConst.COUNT, MAX_ITEM_PER_REQUEST));
                                break;
                        }

                        isLoading = true;
                        mLoadingProgressBar.setVisibility(View.VISIBLE);

                        request.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                VkAudioArray responseAudio = (VkAudioArray) response.parsedModel;

                                Log.d("myLogs", "onLoadMoreResult");

                                audios.addAll(responseAudio);
                                mAdapter.notifyDataSetChanged();

                                isLoading = false;
                                mLoadingProgressBar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError(VKError error) {
                                isLoading = false;
                                mLoadingProgressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }

                    @Override
                    public boolean hasLoadedAllItems() {
                        return false;
                    }
                }).start();

                mRecyclerView.addOnItemTouchListener(new RecyclerClickListener(getActivity(), new RecyclerClickListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        ((MainActivity) getActivity()).playMusic(audios, position);
                    }
                }));
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                Log.d(LOG_TAG, "attempt failed TabLayout Page");
            }
        });*/

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void askForPermission() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, REQUEST_CODE_READ_EXT);

            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, REQUEST_CODE_READ_EXT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(getActivity(), permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                //Location
                case REQUEST_CODE_READ_EXT: {
                }
            }
        }
    }
}