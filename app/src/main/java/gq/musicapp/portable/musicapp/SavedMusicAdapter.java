package gq.musicapp.portable.musicapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by Portable on 04.01.2017.
 */

public class SavedMusicAdapter extends RecyclerView.Adapter<SavedMusicAdapter.SavedMusicViewHolder> implements RealmChangeListener {
    private RealmResults<Music> mAudioArray;

    private boolean mBound = false;
    private ServiceConnection mServiceConnection;
    private MusicService mMusicService;
    private Context mContext;
    private boolean mIsDeleted = false;

    public SavedMusicAdapter(RealmResults<Music> audioArray, Context context) {
        this.mAudioArray = audioArray;
        mAudioArray.addChangeListener(this);
        mContext = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMusicService = ((MusicService.MusicBinder) service).getService();
                mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        Intent intent = new Intent(mContext, MusicService.class);

        mContext.startService(intent);
        mContext.bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        if (mBound) {
            mContext.unbindService(mServiceConnection);
            mBound = false;
        }

        super.onDetachedFromRecyclerView(recyclerView);
    }

    public void setAudios(RealmResults<Music> audioArray) {
        this.mAudioArray = audioArray;
    }

    @Override
    public SavedMusicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.media_list_item, parent, false);
        SavedMusicViewHolder viewHolder = new SavedMusicViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SavedMusicViewHolder holder, final int position) {
        holder.mArtist.setText(mAudioArray.get(position).getArtist());
        holder.mTitle.setText(mAudioArray.get(position).getTitle());
        holder.mDuration.setText(MusicUtil.convertSeconds(mAudioArray.get(position).getDuration()));

        holder.mListItemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.playMusic(mAudioArray, position);
            }
        });

        holder.mMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = mAudioArray.get(position).getUrl();

                File file = new File(Uri.parse(url).getPath());

                if (file.delete()) {

                    Realm realm = Realm.getDefaultInstance();

                    RealmResults<Music> results = realm.where(Music.class).equalTo("url", url).findAll();
                    realm.beginTransaction();
                    mIsDeleted = results.deleteAllFromRealm();
                    realm.commitTransaction();

                    if (mIsDeleted) {
                        Toast.makeText(mContext, mContext.getString(R.string.music_deleted), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.was_not_removed), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.was_not_removed), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAudioArray.size();
    }

    @Override
    public void onChange(Object element) {
        notifyDataSetChanged();
    }

    public class SavedMusicViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mArtist;
        public TextView mDuration;
        public ImageView mMoreOptions;
        public RelativeLayout mListItemContainer;

        public SavedMusicViewHolder(View view) {
            super(view);

            mTitle = (TextView) view.findViewById(R.id.title);
            mArtist = (TextView) view.findViewById(R.id.artist);
            mDuration = (TextView) view.findViewById(R.id.duration);
            mMoreOptions = (ImageView) view.findViewById(R.id.more_options);
            mMoreOptions.setImageResource(R.drawable.ic_delete_black_24dp);
            mListItemContainer = (RelativeLayout) view.findViewById(R.id.list_item_container);
        }
    }
}
