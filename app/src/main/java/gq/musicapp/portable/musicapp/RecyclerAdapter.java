package gq.musicapp.portable.musicapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by Portable on 06.10.2016.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private List<Music> mAudioArray;
    private int mPage;
    private boolean mBound = false;
    private ServiceConnection mServiceConnection;
    private MusicService mMusicService;
    private Context mContext;

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMusicService = ((MusicService.MusicBinder) service).getService();
                mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        Intent intent = new Intent(mContext, MusicService.class);

        mContext.startService(intent);
        mContext.bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        if (mBound) {
            mContext.unbindService(mServiceConnection);
            mBound = false;
        }

        super.onDetachedFromRecyclerView(recyclerView);
    }

    public RecyclerAdapter(List<Music> audioArray, int page, Context context) {
        this.mAudioArray = audioArray;
        mPage = page;
        mContext = context;
    }

    public void setAudios(List<Music> audioArray) {
        this.mAudioArray = audioArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.media_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mArtist.setText(mAudioArray.get(position).getArtist());
        holder.mTitle.setText(mAudioArray.get(position).getTitle());
        holder.mDuration.setText(MusicUtil.convertSeconds(mAudioArray.get(position).getDuration()));

        holder.mListItemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.playMusic(mAudioArray, position);
            }
        });

        holder.mMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicService.downloadMusic(mAudioArray.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAudioArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mArtist;
        public TextView mDuration;
        public ImageView mMoreOptions;
        public RelativeLayout mListItemContainer;

        public ViewHolder(View view) {
            super(view);

            mTitle = (TextView) view.findViewById(R.id.title);
            mArtist = (TextView) view.findViewById(R.id.artist);
            mDuration = (TextView) view.findViewById(R.id.duration);
            mMoreOptions = (ImageView) view.findViewById(R.id.more_options);
            mMoreOptions.setImageResource(R.drawable.ic_down_arrow);
            mListItemContainer = (RelativeLayout) view.findViewById(R.id.list_item_container);
        }
    }
}
