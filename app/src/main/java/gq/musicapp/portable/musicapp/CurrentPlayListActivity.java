package gq.musicapp.portable.musicapp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.clockbyte.admobadapter.expressads.AdmobExpressRecyclerAdapterWrapper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;

import static gq.musicapp.portable.musicapp.MusicUtil.getAdRequest;
import static gq.musicapp.portable.musicapp.MusicUtil.getAdapterWrapper;

public class CurrentPlayListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private boolean mBound = false;
    private ServiceConnection mServiceConnection;
    private MusicService mMusicService;
    private AdView mAdView;
    private AdmobExpressRecyclerAdapterWrapper mAdapterWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_play_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapterWrapper = getAdapterWrapper(this);

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMusicService = ((MusicService.MusicBinder) service).getService();
                mBound = true;
                mAdapter = new RecyclerAdapter(mMusicService.getPlayList(),1, CurrentPlayListActivity.this);
                /*mAdapterWrapper.setAdapter(mAdapter);
                mRecyclerView.setAdapter(mAdapterWrapper);*/
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        mRecyclerView.addOnItemTouchListener(new RecyclerClickListener(this, new RecyclerClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mMusicService.playMusic(position);
            }
        }));

        Intent intent = new Intent(this, MusicService.class);

        startService(intent);
        bindService(intent, mServiceConnection, BIND_AUTO_CREATE);

        // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
        // values/strings.xml.
        mAdView = (AdView) findViewById(R.id.ad_view);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }
        });

        // Start loading the ad in the background.
        mAdView.loadAd(getAdRequest());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    protected void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }

        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }

        super.onDestroy();
    }
}
