package gq.musicapp.portable.musicapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.clockbyte.admobadapter.expressads.AdmobExpressRecyclerAdapterWrapper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static gq.musicapp.portable.musicapp.MusicUtil.getAdRequest;
import static gq.musicapp.portable.musicapp.MusicUtil.getAdapterWrapper;

public class SearchActivity extends AppCompatActivity implements MusicService.ServiceCallbacksMiniController {

    private EditText mSearchEditText;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private boolean mBound = false;
    private ServiceConnection mServiceConnection;
    private MusicService mMusicService;
    private List<Music> mAudios;
    private LinearLayout mMiniController;
    private TextView mTitle;
    private TextView mArtist;
    private ImageView mMusicImage;
    private AdView mAdView;
    private boolean isLoading = false;
    private BaseAttacher mBaseAttacher;
    private ProgressBar mLoadingProgressBar;
    private String mSearchMusic;
    private OkHttpClient mOkHttpClient;
    private String mAccessToken;
    private String mUser_Id;
    private SharedPreferences mSharedPreferences;
    private Handler mHandler;
    private int prevAudioCount;
    private ProgressBar mProgressBar;
    private int mLastRequestId;
    private ImageView mQuickClear;
    private AdmobExpressRecyclerAdapterWrapper mAdapterWrapper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLoadingProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);
        mSearchEditText = (EditText) findViewById(R.id.search_edit_text);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(false);
        mMiniController = (LinearLayout) findViewById(R.id.music_controller);
        mArtist = (TextView) findViewById(R.id.artist);
        mTitle = (TextView) findViewById(R.id.title);
        mMusicImage = (ImageView) findViewById(R.id.music_image);
        mOkHttpClient = new OkHttpClient();
        mSharedPreferences = getSharedPreferences(AppConst.APP_PREF, MODE_PRIVATE);
        mAccessToken = mSharedPreferences.getString("access_token", "");
        mUser_Id = mSharedPreferences.getString("user_id", "");
        mHandler = new Handler();
        mQuickClear = (ImageView) findViewById(R.id.quick_clear);
        mAdapterWrapper = getAdapterWrapper(this);

        mRecyclerView.addOnItemTouchListener(new RecyclerClickListener(this, new RecyclerClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mMusicService.playMusic(mAudios, position);
            }
        }));

        mMiniController.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, MusicControllerActivity.class);
                startActivity(intent);
            }
        });

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMusicService = ((MusicService.MusicBinder) service).getService();
                mMusicService.setServiceCallbacksSearchActivity(SearchActivity.this);
                mBound = true;

                if (mMusicService.getMediaPlayer() != null) {
                    updateMiniController();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        mBaseAttacher = Mugen.with(mRecyclerView, new MugenCallbacks() {

            @Override
            public void onLoadMore() {
                OkHttpClient innerOkHttp = new OkHttpClient();
                mSearchMusic = mSearchEditText.getText().toString();
                prevAudioCount = mAudios.size();

                try {
                    mSearchMusic = URLEncoder.encode(mSearchMusic, "UTF-8");

                    final Request innerRequest = new Request.Builder()
                            .url(AppConst.BASE_URL + "audio.search?" + AppConst.SECOND_PART_URL + mAccessToken + "&q=" + mSearchMusic + "&offset=" + mAudios.size() + "&count=200")
                            .build();

                    mLoadingProgressBar.setVisibility(View.VISIBLE);


                    innerOkHttp.newCall(innerRequest).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {

                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            final String innerResult = response.body().string();

                            try {
                                JSONArray innerJsonArray = new JSONObject(innerResult)
                                        .getJSONObject("response")
                                        .getJSONArray("items");

                                Type listType = new TypeToken<ArrayList<Music>>() {
                                }.getType();
                                Gson gson = new Gson();
                                List<Music> tempAudios = gson.fromJson(innerJsonArray.toString(), listType);

                                mAudios.addAll(tempAudios);

                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mAdapter.notifyDataSetChanged();
                                        mLoadingProgressBar.setVisibility(View.INVISIBLE);
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        }).start();

        Intent intent = new Intent(this, MusicService.class);

        startService(intent);
        bindService(intent, mServiceConnection, BIND_AUTO_CREATE);

        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mSearchEditText.getText().toString().equals("")) {
                    mQuickClear.setVisibility(View.INVISIBLE);
                } else {
                    mQuickClear.setVisibility(View.VISIBLE);
                }
            }
        });

        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();

                    mSearchMusic = mSearchEditText.getText().toString();

                    try {
                        mSearchMusic = URLEncoder.encode(mSearchMusic, "UTF-8");

                        final Request request = new Request.Builder()
                                .url(AppConst.BASE_URL + "audio.search?" + AppConst.SECOND_PART_URL + mAccessToken + "&q=" + mSearchMusic + "&count=200")
                                .build();

                        mLastRequestId = request.hashCode();
                        mRecyclerView.setVisibility(View.INVISIBLE);
                        mProgressBar.setVisibility(View.VISIBLE);
                        mOkHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                if (mLastRequestId == request.hashCode()) {
                                    String result = response.body().string();

                                    try {
                                        JSONArray jsonArray = new JSONObject(result)
                                                .getJSONObject("response")
                                                .getJSONArray("items");

                                        Type listType = new TypeToken<ArrayList<Music>>() {
                                        }.getType();
                                        Gson gson = new Gson();
                                        mAudios = gson.fromJson(jsonArray.toString(), listType);

                                        mHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (mAdapter != null) {
                                                    mAdapter = new RecyclerAdapter(mAudios, 1, SearchActivity.this);
                                                    /*mAdapterWrapper.setAdapter(mAdapter);
                                                    mRecyclerView.swapAdapter(mAdapterWrapper, false);*/
                                                    mRecyclerView.swapAdapter(mAdapter, false);
                                                } else {
                                                    mAdapter = new RecyclerAdapter(mAudios, 1, SearchActivity.this);
                                                    /*mAdapterWrapper.setAdapter(mAdapter);
                                                    mRecyclerView.setAdapter(mAdapterWrapper);*/
                                                    mRecyclerView.setAdapter(mAdapter);
                                                }

                                                mProgressBar.setVisibility(View.INVISIBLE);
                                                mRecyclerView.setVisibility(View.VISIBLE);
                                            }
                                        });

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    return true;
                }

                return false;
            }
        });

        mQuickClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchEditText.setText("");
            }
        });

        // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
        // values/strings.xml.
        mAdView = (AdView) findViewById(R.id.ad_view);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }
        });

        // Start loading the ad in the background.
        mAdView.loadAd(getAdRequest());
    }

    private void performSearch() {
        mSearchEditText.clearFocus();

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mSearchEditText.getWindowToken(), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    protected void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }

        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }

        super.onDestroy();
    }

    @Override
    public void updateMiniController() {
        mMiniController.setVisibility(View.VISIBLE);

        mArtist.setText(mMusicService.getCurrentMusicArtist());
        mTitle.setText(mMusicService.getCurrentMusicTitle());

        /*updateMiniControllerImage(mMusicService.getmMusicImage());*/
        updateMiniControllerImage(mMusicService.getmMusicImage());
    }

    @Override
    public void updateMiniControllerImage(Bitmap musicImage) {
        if (musicImage != null) {
            mMusicImage.setImageBitmap(musicImage);
        } else {
            mMusicImage.setImageBitmap(null);
        }
    }
}