package gq.musicapp.portable.musicapp;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import static gq.musicapp.portable.musicapp.PageFragment.ARG_PAGE;

/**
 * Created by Portable on 05.10.2016.
 */

public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {
    final int PAGE_COUNT = 4;
    private String tabTitle[] = new String[]{"Tab 1", "Tab 2", "Tab 3", "Tab 4"};
    private Context context;

    public FragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //return tabTitle[position];

        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        PageFragment pageFragment = (PageFragment) object;
        int page = pageFragment.getArguments().getInt(ARG_PAGE);

        if (page == 1) {
            return POSITION_NONE;
        }

        return POSITION_UNCHANGED;
    }
}
