package gq.musicapp.portable.musicapp;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Portable on 07.10.2016.
 */

public class RecyclerClickListener implements RecyclerView.OnItemTouchListener {
    public interface ClickListener {
        void onClick(View view, int position);
    }

    private GestureDetectorCompat mGestureDetectorCompat;
    private ClickListener mClickListener;

    public RecyclerClickListener(Context context, ClickListener clickListener) {
        mClickListener = clickListener;

        mGestureDetectorCompat = new GestureDetectorCompat(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());

        if (child != null && mClickListener != null && mGestureDetectorCompat.onTouchEvent(e)) {
            mClickListener.onClick(child, rv.getChildAdapterPosition(child));
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
