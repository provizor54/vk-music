package gq.musicapp.portable.musicapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static gq.musicapp.portable.musicapp.AppConst.APP_PREF;


public class LoginActivity extends AppCompatActivity {

    private Button mSignIn;
    private String mLogin;
    private String mPassword;
    private EditText mLogET;
    private EditText mPassET;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences mSharedPreferences;
    private Handler mHandler;
    private TextView mViaWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSignIn = (Button) findViewById(R.id.sign_in);
        mLogET = (EditText) findViewById(R.id.login);
        mPassET = (EditText) findViewById(R.id.password);
        mViaWebView = (TextView) findViewById(R.id.via_web_view);
        mSharedPreferences = getSharedPreferences(APP_PREF, MODE_PRIVATE);
        mHandler = new Handler();

        if (mSharedPreferences.contains("access_token")) {
            Intent intent = new Intent(this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        }

        mPassET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    mSignIn.callOnClick();

                    return true;
                }

                return false;
            }
        });

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                try {
                    mLogin = URLEncoder.encode(mLogET.getText().toString(), "UTF-8");
                    mPassword = URLEncoder.encode(mPassET.getText().toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                OkHttpClient okHttpClient = new OkHttpClient();

                Request request = new Request.Builder()
                        .url("")
                        .build();


                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String result = response.body().string();

                        try {
                            JSONObject json = new JSONObject(result);

                            if (json.has("error")) {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(LoginActivity.this, "Неверный логин или пароль", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else {
                                mEditor = getSharedPreferences("data_store", MODE_PRIVATE).edit();
                                mEditor.putString("access_token", json.getString("access_token"));
                                mEditor.putString("user_id", json.getString("user_id"));
                                mEditor.apply();

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        mViaWebView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, WebAuthActivity.class);

                startActivity(intent);
            }
        });
    }

    private void hideKeyboard() {
        /*mPassET.clearFocus();

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mPassET.getWindowToken(), 0);*/

        View view = this.getCurrentFocus();

        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
