package gq.musicapp.portable.musicapp;

import io.realm.RealmObject;

/**
 * Created by Portable on 26.12.2016.
 */

public class Music extends RealmObject {
    private String artist;
    private String title;
    private int duration;
    private String id;
    private String owner_id;
    private String url;

    public Music() {

    }

    public Music(String artist, String title, int duration, String id, String owner_id, String url) {
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        this.id = id;
        this.owner_id = owner_id;
        this.url = url;
    }

    public Music(String artist, String title, String url, int duration) {
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        this.url = url;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
