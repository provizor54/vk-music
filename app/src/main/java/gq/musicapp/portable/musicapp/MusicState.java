package gq.musicapp.portable.musicapp;

import java.util.List;

/**
 * Created by Portable on 13.11.2016.
 */

public class MusicState {
    public static final int UNKOWN = -123;
    public int lastKnownAudioFocusState;
    public boolean audioFocusGranted;
    public boolean wasPlayingWhenTransientLoss;
    public boolean released;
    public int PLAY;
    public List<Music> audio;
    public int mCurrentPosition;
    public int mCurrentTime;

    public static class PlayState {
        public static final int PLAY = 1;
        public static final int PAUSE = 2;
    }
}
