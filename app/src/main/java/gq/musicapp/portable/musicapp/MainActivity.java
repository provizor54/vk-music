package gq.musicapp.portable.musicapp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static gq.musicapp.portable.musicapp.AppConst.APP_PREF;
import static gq.musicapp.portable.musicapp.AppConst.BASE_URL;
import static gq.musicapp.portable.musicapp.AppConst.SECOND_PART_URL;
import static gq.musicapp.portable.musicapp.MusicUtil.getAdRequest;

public class MainActivity extends AppCompatActivity
        implements MusicService.ServiceCallbacksMiniController, MusicService.ServiceCallbacksUpdateList, NavigationView.OnNavigationItemSelectedListener {

    private boolean mBound = false;
    private ServiceConnection mServiceConnection;
    private MusicService mMusicService;
    private LinearLayout mMiniController;
    private TextView mTitle;
    private TextView mArtist;
    public static int USER_ID;
    private ImageView mAvatar;
    private TextView mFullName;
    private ImageView mMusicImage;
    private TabLayout tabLayout;
    private AdView mAdView;
    private String mAccessToken;
    private String mUser_Id;
    private SharedPreferences mSharedPreferences;
    private OkHttpClient mOkHttpClient;
    private Handler mHandler;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        mAvatar = (ImageView) header.findViewById(R.id.avatar);
        mFullName = (TextView) header.findViewById(R.id.full_name);
        mMusicImage = (ImageView) findViewById(R.id.music_image);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_sd_storage_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_queue_music_black_24dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_grade_black_24dp);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_thumb_up_black_24dp);

        mMiniController = (LinearLayout) findViewById(R.id.music_controller);
        mMiniController.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MusicControllerActivity.class);
                startActivity(intent);
            }
        });

        mArtist = (TextView) findViewById(R.id.artist);
        mTitle = (TextView) findViewById(R.id.title);
        mSharedPreferences = getSharedPreferences(AppConst.APP_PREF, MODE_PRIVATE);
        mAccessToken = mSharedPreferences.getString("access_token", "");
        mUser_Id = mSharedPreferences.getString("user_id", "");
        mOkHttpClient = new OkHttpClient();
        mHandler = new Handler();

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMusicService = ((MusicService.MusicBinder) service).getService();
                mMusicService.setServiceCallbacksMainActivity(MainActivity.this);
                mMusicService.setServiceCallbacksUpdateDownloadedList(MainActivity.this);
                mBound = true;

                updateMiniController();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        Intent intent = new Intent(this, MusicService.class);

        startService(intent);
        bindService(intent, mServiceConnection, BIND_AUTO_CREATE);

        Request request = new Request.Builder()
                .url(BASE_URL + "users.get?" + SECOND_PART_URL + mAccessToken + "&user_ids=" + mUser_Id + "&fields=photo_200")
                .build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();

                try {
                    JSONObject jsonObject = new JSONObject(result)
                            .getJSONArray("response")
                            .getJSONObject(0);
                    final String photoUrl = jsonObject.getString("photo_200");
                    final String first_name = jsonObject.getString("first_name");
                    final String last_name = jsonObject.getString("last_name");

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mFullName.setText(first_name + " " + last_name);

                            /*new Picasso.Builder(MainActivity.this)
                                    .downloader(new OkHttp3Downloader(MainActivity.this, Integer.MAX_VALUE))
                                    .build()
                                    .with(MainActivity.this)
                                    .load(photoUrl)
                                    .into(mAvatar);*/

                            Picasso.with(MainActivity.this)
                                    .load(photoUrl)
                                    .into(mAvatar);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference();

        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("musicapp")) {
                    if ((boolean) dataSnapshot.child("musicapp").child("hasUpdate").getValue()) {

                        getSharedPreferences(APP_PREF, MODE_PRIVATE).edit()
                                .putBoolean("hasUpdate", true)
                                .putString("url", dataSnapshot.child("musicapp")
                                        .child("url").getValue().toString())
                                .putString("googlePlayUrl", dataSnapshot.child("musicapp")
                                        .child("googlePlayUrl").getValue().toString())
                                .commit();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
        // values/strings.xml.
        mAdView = (AdView) findViewById(R.id.ad_view);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }
        });

        // Start loading the ad in the background.
        mAdView.loadAd(getAdRequest());

        //showRatingDialog();

        if (mSharedPreferences.getInt("rate_dialog_showed", 0) == 0) {
            MusicUtil.showRateDialog(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    protected void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }

        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();


        updateMiniController();
    }

    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }

        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_search) {
            startActivity(new Intent(this, SearchActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.saved_music:
                tabLayout.getTabAt(0).select();
                break;
            case R.id.my_music:
                tabLayout.getTabAt(1).select();
                break;
            case R.id.popular_music:
                tabLayout.getTabAt(2).select();
                break;
            case R.id.recommendation_music:
                tabLayout.getTabAt(3).select();
                break;
            case R.id.rate_me:
            case R.id.feedback:
                openGooglePlay();
                break;
            case R.id.invite:
                String url = "https://play.google.com/store/apps/details?id=" + getPackageName();
                String message = getString(R.string.invite_text) + "\n\n" + url;

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                sendIntent.setType("text/plain");

                startActivity(sendIntent);
                break;
            case R.id.logout:
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.remove("access_token");
                editor.remove("user_id");
                editor.apply();

                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void playMusic(List<Music> audios, int position) {
        if (mBound) {
            mMusicService.playMusic(audios, position);
        }
    }

    @Override
    public void updateMiniController() {

        if (mMusicService != null && mMusicService.getMediaPlayer() != null) {
            mMiniController.setVisibility(View.VISIBLE);

            mArtist.setText(mMusicService.getCurrentMusicArtist());
            mTitle.setText(mMusicService.getCurrentMusicTitle());

            updateMiniControllerImage(mMusicService.getmMusicImage());
        }
    }

    @Override
    public void updateDownloadedList() {
        mViewPager.getAdapter().notifyDataSetChanged();

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_sd_storage_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_queue_music_black_24dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_grade_black_24dp);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_thumb_up_black_24dp);
    }

    @Override
    public void updateMiniControllerImage(Bitmap musicImage) {
        if (musicImage != null) {
            mMusicImage.setImageBitmap(musicImage);
        } else {
            mMusicImage.setImageBitmap(null);
        }
    }

    private void showRatingDialog() {
        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .session(7)
                .title(getString(R.string.rating_dialog_title))
                .titleTextColor(R.color.black)
                .positiveButtonText(getString(R.string.positive_button))
                .negativeButtonText(getString(R.string.negative_button))
                .positiveButtonTextColor(R.color.white)
                .negativeButtonTextColor(R.color.grey_500)
                .ratingBarColor(R.color.yellow)
                .positiveButtonBackgroundColor(R.drawable.button_selector_positive)
                .negativeButtonBackgroundColor(R.drawable.button_selector_negative)
                .build();

        ratingDialog.show();
    }

    private void openGooglePlay() {
        String url = "https://play.google.com/store/apps/details?id=" + getPackageName();
        String googlePlayUrl = "market://details?id=" + getPackageName();

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(googlePlayUrl)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }
}
