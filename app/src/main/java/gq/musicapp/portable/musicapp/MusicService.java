package gq.musicapp.portable.musicapp;

import android.app.DownloadManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.devbrackets.android.exomedia.EMAudioPlayer;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import wseemann.media.FFmpegMediaMetadataRetriever;

import static gq.musicapp.portable.musicapp.AppConst.BASE_URL;
import static gq.musicapp.portable.musicapp.AppConst.LOCAL_STORE_URL;
import static gq.musicapp.portable.musicapp.AppConst.SECOND_PART_URL;

public class MusicService extends Service implements OnPreparedListener, OnErrorListener, OnCompletionListener, AudioManager.OnAudioFocusChangeListener {

    public interface ServiceCallbacksMusicController {
        void updateUIForMusicController();

        void updateUIBroadcast();

        void updateUIaddToPlaylist(int value);

        void updateUIMusicImage(Bitmap musicImage);

        void updateUIDownloadMusic(boolean enabled);

        void updateUIPlayToggle(boolean isPlay);

        void updateUIinMyPlayList(boolean isInMyPlayList);
    }

    public interface ServiceCallbacksMiniController {
        void updateMiniController();

        void updateMiniControllerImage(Bitmap musicImage);
    }

    public interface ServiceCallbacksUpdateList {
        void updateDownloadedList();
    }

    private static final int MUSIC_PAUSE = 1;
    public static final int MUSIC_PLAY = 2;
    private static final String ACTION_PLAY_TOGGLE = "com.example.portable.vkmusic.ACTION.PLAY_TOGGLE";
    private static final String ACTION_PLAY_PREVIOUS = "com.example.portable.vkmusic.ACTION.PLAY_PREVIOUS";
    private static final String ACTION_PLAY_NEXT = "com.example.portable.vkmusic.ACTION.PLAY_NEXT";
    private static final String ACTION_STOP_SERVICE = "com.example.portable.vkmusic.ACTION.STOP_SERVICE";
    private static final int NOTIFICATION_ID = 3;
    public static final int ADD_TO_PLAYLIST = 4;
    public static final int REMOVE_FROM_PLAYLIST = 5;
    public static final float VOLUME_LOWER = 0.2f;
    public static final float VOLUME_NORMAL = 1.0f;

    private MusicBinder mMusicBinder;
    private EMAudioPlayer mMediaPlayer;
    private MainActivity mMainActivity;
    private List<Music> mCurrentAudioList;
    private int mCurrentPosition;
    private boolean isShuffling = false;
    private boolean isLooping = false;
    private Random random;
    private ServiceCallbacksMusicController mServiceCallbacksMusicController;
    private ServiceCallbacksMiniController mServiceCallbacksMainActivity;
    private ServiceCallbacksMiniController mServiceCallbacksSearchActivity;
    private ServiceCallbacksUpdateList mServiceCallbacksUpdateList;
    private RemoteViews mContentViewBig;
    private RemoteViews mContentViewSmall;
    private boolean mIsBroadcast = false;
    private Bitmap mMusicImage;
    private MusicImageTask mMusicImageTask;
    private DownloadManager downloadManager;
    private long downloadReference;
    private HeadsetReceiver mHeadsetReceiver;
    private boolean mIsWasPlaying = false;
    private AudioManager mAudioManager;
    private int requestGranted;
    private MusicState mMusicState;
    private String mAccessToken;
    private String mUser_Id;
    private SharedPreferences mSharedPreferences;
    private OkHttpClient mOkHttpClient;
    //private Context mContext;
    private Music mPrevMusic;
    private boolean isPressedPrevOrNext = false;
    private Handler mHandler;
    private boolean mIsNotificationClosed = false;
    private HashMap<Long, Music> downloadList;

    @Override
    public void onCreate() {
        super.onCreate();

        mMusicState = new MusicState();
        mMusicBinder = new MusicBinder();
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        random = new Random();
        mMusicImageTask = new MusicImageTask();
        mHeadsetReceiver = new HeadsetReceiver();
        registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerReceiver(mHeadsetReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        mSharedPreferences = getSharedPreferences(AppConst.APP_PREF, MODE_PRIVATE);
        mAccessToken = mSharedPreferences.getString("access_token", "");
        mUser_Id = mSharedPreferences.getString("user_id", "");
        mOkHttpClient = new OkHttpClient();
        mHandler = new Handler();
        downloadList = new HashMap<>();
        //mContext = this;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMusicBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = null;

        if (intent != null)
            action = intent.getAction();

        if (action != null) {
            if (ACTION_PLAY_TOGGLE.equals(action)) {
                pause();
            } else if (ACTION_PLAY_NEXT.equals(action)) {
                nextMusic();
            } else if (ACTION_PLAY_PREVIOUS.equals(action)) {
                previousMusic();
            } else if (ACTION_STOP_SERVICE.equals(action)) {
                if (isPlaying())
                    pause();

                mIsNotificationClosed = true;
                stopForeground(true);
            }
        }

        return START_STICKY;
    }

    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }

    private void updateUI() {
        if (mServiceCallbacksMusicController != null) {
            mServiceCallbacksMusicController.updateUIForMusicController();
        }

        if (mServiceCallbacksMainActivity != null) {
            mServiceCallbacksMainActivity.updateMiniController();
        }

        if (mServiceCallbacksSearchActivity != null) {
            mServiceCallbacksSearchActivity.updateMiniController();
        }
    }

    //update broadcast icon in MusicControllerActivity.class
    private void updateBroadcast() {
        if (mServiceCallbacksMusicController != null) {
            mServiceCallbacksMusicController.updateUIBroadcast();
        }
    }

    public void playMusic(List<Music> audios, int position) {
        mCurrentAudioList = audios;

        playMusic(position);
        /*releaseMP();

        mCurrentPosition = position;
        mCurrentAudioList = audios;

        mMusicImage = null;
        updateUI();
        updateUIDefaultImage();

        if (mMusicState.audioFocusGranted == false) {
            requestGranted = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }

        if (requestGranted == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            try {
                mMediaPlayer = new MediaPlayer();
                mMediaPlayer.setDataSource(mCurrentAudioList.get(position).getUrl());
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setOnPreparedListener(this);
                mMediaPlayer.setOnCompletionListener(this);
                mMediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

    public void playMusic(int position) {
        if (!isLooping()) {
            if (mPrevMusic != null) {
                if (mCurrentPosition == position) {
                    if (mPrevMusic.getUrl().equals(mCurrentAudioList.get(position).getUrl())) {
                        if (isPressedPrevOrNext && mCurrentAudioList.size() == 1) {
                            isPressedPrevOrNext = false;
                        } else {
                            pause();

                            return;
                        }
                    }
                }
            }
        }

        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
        } else {
            mMediaPlayer = new EMAudioPlayer(this);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setOnPreparedListener(MusicService.this);
            mMediaPlayer.setOnCompletionListener(MusicService.this);
        }

        mCurrentPosition = position;

        mMusicImage = null;
        mIsNotificationClosed = false;
        updateUI();
        showNotification();
        //updateUIDefaultImage();

        if (mMusicState.audioFocusGranted == false) {
            requestGranted = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }

        if (requestGranted == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mMediaPlayer.setDataSource(this, Uri.parse(mCurrentAudioList.get(mCurrentPosition).getUrl()));
            mMediaPlayer.prepareAsync();
        }

        mPrevMusic = getCurrentAudio();

        /*mCurrentPosition = position;

        mMusicImage = null;
        updateUI();
        updateUIDefaultImage();

        mMediaPlayer.reset();

        if (mMusicState.audioFocusGranted == false) {
            requestGranted = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }

        if (requestGranted == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            try {
                mMediaPlayer.setDataSource(mCurrentAudioList.get(mCurrentPosition).getUrl());
                mMediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

    public void nextMusic() {
        /*mCurrentPosition++;*/
        int nextPosition = mCurrentPosition + 1;

        if (nextPosition > mCurrentAudioList.size() - 1) {
            nextPosition = 0;
        }

        isPressedPrevOrNext = true;

        /*if (mServiceCallbacksMusicController != null) {
            mServiceCallbacksMusicController.updateUIinMyPlayList(isInMyPlaylist());
        }*/

        playMusic(nextPosition);
    }

    public void previousMusic() {
        /*mCurrentPosition--;*/

        int prevPosition = mCurrentPosition - 1;

        if (prevPosition < 0) {
            prevPosition = mCurrentAudioList.size() - 1;
        }

        isPressedPrevOrNext = true;

        /*if (mServiceCallbacksMusicController != null) {
            mServiceCallbacksMusicController.updateUIinMyPlayList(isInMyPlaylist());
        }*/

        playMusic(prevPosition);
    }

    //TogglePlayPause
    public int pause() {

        if (mMusicState.audioFocusGranted == false) {
            requestGranted = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }

        mIsNotificationClosed = false;

        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();

            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIPlayToggle(isPlaying());
            }

            showNotification();

            return MUSIC_PAUSE;
        } else {
            mMediaPlayer.start();

            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIPlayToggle(isPlaying());
            }

            showNotification();

            return MUSIC_PLAY;
        }
    }

    public boolean isPlaying() {
        if (mMediaPlayer != null) {
            return mMediaPlayer.isPlaying();
        }

        return false;
    }

    // TODO: 27.12.2016
    /*public void setLooping(boolean looping) {
        //mMediaPlayer.setLooping(looping);
    }*/

    public void setLooping(boolean looping) {
        isLooping = looping;
    }

    public boolean isLooping() {
        //return mMediaPlayer.isLooping();

        return isLooping;
    }

    public boolean isShuffling() {
        return isShuffling;
    }

    private void shuffleMusic() {
        int position;

        do {
            position = random.nextInt(mCurrentAudioList.size());
        } while (position == mCurrentPosition);

        playMusic(position);
    }

    public void setShuffle(boolean shuffling) {
        isShuffling = shuffling;
    }

    private void releaseMP() {
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public EMAudioPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    public int getDuration() {
        return mCurrentAudioList.get(mCurrentPosition).getDuration() * 1000;
    }

    public String getCurrentMusicTitle() {
        return mCurrentAudioList.get(mCurrentPosition).getTitle();
    }

    public String getCurrentMusicArtist() {
        return mCurrentAudioList.get(mCurrentPosition).getArtist();
    }

    public Bitmap getmMusicImage() {
        return mMusicImage;
    }

    /*@Override
    public void onPrepared(MediaPlayer mp) {
        mMediaPlayer.start();
        mMusicImageTask.cancel(true);
        mMusicImageTask = new MusicImageTask();
        mMusicImageTask.execute(getCurrentAudio());

        showNotification();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

        if (isShuffling()) {
            shuffleMusic();
        } else {
            if ((mCurrentAudioList.size() - 1) == mCurrentPosition) {
                mCurrentPosition = 0;
            }

            mCurrentPosition++;

            playMusic(mCurrentPosition);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Toast.makeText(getApplicationContext(), "MediaPlayer OnError", Toast.LENGTH_SHORT).show();

        return false;
    }*/

    @Override
    public void onPrepared() {
        mMediaPlayer.start();

        mMusicImageTask.cancel(true);
        mMusicImageTask = new MusicImageTask();
        mMusicImageTask.execute(getCurrentAudio());

        if (mServiceCallbacksMusicController != null) {
            mServiceCallbacksMusicController.updateUIPlayToggle(isPlaying());
        }

        showNotification();
    }

    @Override
    public void onCompletion() {
        if (isLooping()) {
            playMusic(mCurrentPosition);
        } else if (isShuffling()) {
            shuffleMusic();
        } else {
            if ((mCurrentAudioList.size() - 1) == mCurrentPosition) {
                mCurrentPosition = 0;
            } else {
                mCurrentPosition++;
            }

            playMusic(mCurrentPosition);
        }
    }

    @Override
    public boolean onError() {
        return false;
    }

    public void seekTo(int position) {
        mMediaPlayer.seekTo(position);
    }

    @Override
    public void onDestroy() {
        releaseMP();
        unregisterReceiver(downloadReceiver);
        unregisterReceiver(mHeadsetReceiver);

        super.onDestroy();
    }

    public void setServiceCallbacksMusicController(ServiceCallbacksMusicController serviceCallbacks) {
        mServiceCallbacksMusicController = serviceCallbacks;
    }

    public void setServiceCallbacksMainActivity(ServiceCallbacksMiniController serviceCallbacks) {
        mServiceCallbacksMainActivity = serviceCallbacks;
    }

    public void setServiceCallbacksSearchActivity(ServiceCallbacksMiniController serviceCallbacks) {
        mServiceCallbacksSearchActivity = serviceCallbacks;
    }

    public void setServiceCallbacksUpdateDownloadedList(ServiceCallbacksUpdateList serviceCallbacks) {
        mServiceCallbacksUpdateList = serviceCallbacks;
    }

    private void showNotification() {
        if (!mIsNotificationClosed) {
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MusicControllerActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

            android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(contentIntent)
                    .setCustomContentView(getSmallContentView())
                    .setCustomBigContentView(getBigContenView())
                    .setPriority(NotificationCompat.PRIORITY_MAX);

            if (isPlaying()) {
                notificationBuilder.setOngoing(true);
            } else {
                notificationBuilder.setOngoing(false);
            }

            startForeground(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    private RemoteViews getSmallContentView() {
        if (mContentViewSmall == null) {
            mContentViewSmall = new RemoteViews(getPackageName(), R.layout.notification_custom_layout);
            setUpRemoteView(mContentViewSmall);
        }

        updateSmallRemoteView(mContentViewSmall);

        return mContentViewSmall;
    }

    private RemoteViews getBigContenView() {
        if (mContentViewBig == null) {
            mContentViewBig = new RemoteViews(getPackageName(), R.layout.notification_custom_expanded_layout);
            setUpRemoteView(mContentViewBig);
        }

        updateBigRemoteView(mContentViewBig);

        return mContentViewBig;
    }

    private void setUpRemoteView(RemoteViews remoteView) {
        remoteView.setOnClickPendingIntent(R.id.notification_close, getPendingIntent(ACTION_STOP_SERVICE));
        remoteView.setOnClickPendingIntent(R.id.notification_previous, getPendingIntent(ACTION_PLAY_PREVIOUS));
        remoteView.setOnClickPendingIntent(R.id.notification_play_toggle, getPendingIntent(ACTION_PLAY_TOGGLE));
        remoteView.setOnClickPendingIntent(R.id.notification_next, getPendingIntent(ACTION_PLAY_NEXT));
    }

    private void updateSmallRemoteView(RemoteViews remoteView) {
        remoteView.setTextViewText(R.id.notification_title, getCurrentMusicTitle());
        remoteView.setTextViewText(R.id.notification_artist, getCurrentMusicArtist());

        if (mMusicImage != null) {
            remoteView.setImageViewBitmap(R.id.notification_image, mMusicImage);
        } else {
            remoteView.setImageViewResource(R.id.notification_image, R.drawable.notification_small);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            remoteView.setImageViewResource(R.id.notification_play_toggle, isPlaying()
                    ? R.drawable.ic_pause_black_36dp : R.drawable.ic_play_arrow_black_36dp);
        } else {
            remoteView.setImageViewResource(R.id.notification_play_toggle, isPlaying()
                    ? R.drawable.ic_pause_white_36dp : R.drawable.ic_play_arrow_white_36dp);
        }
    }

    private void updateBigRemoteView(RemoteViews remoteView) {
        remoteView.setTextViewText(R.id.notification_title, getCurrentMusicTitle());
        remoteView.setTextViewText(R.id.notification_artist, getCurrentMusicArtist());

        if (mMusicImage != null) {
            remoteView.setImageViewBitmap(R.id.notification_image, mMusicImage);
        } else {
            remoteView.setImageViewResource(R.id.notification_image, R.drawable.notification_big);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            remoteView.setImageViewResource(R.id.notification_play_toggle, isPlaying()
                    ? R.drawable.ic_pause_black_48dp : R.drawable.ic_play_arrow_black_48dp);
        } else {
            remoteView.setImageViewResource(R.id.notification_play_toggle, isPlaying()
                    ? R.drawable.ic_pause_white_48dp : R.drawable.ic_play_arrow_white_48dp);
        }
    }

    private PendingIntent getPendingIntent(String action) {
        return PendingIntent.getService(this, 0, new Intent(this, MusicService.class).setAction(action), 0);
    }

    public void broadcastMusic() {
        if (mIsBroadcast) {
            mIsBroadcast = false;
            updateBroadcast();

            Request request = new Request.Builder()
                    .url(BASE_URL + "audio.setBroadcast?" + SECOND_PART_URL + mAccessToken)
                    .build();

            mOkHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                }
            });
        } else {
            mIsBroadcast = true;
            updateBroadcast();

            Request request = new Request.Builder()
                    .url(BASE_URL + "audio.setBroadcast?" + SECOND_PART_URL + mAccessToken
                            + "&audio=" + getCurrentAudio().getOwner_id()
                            + "_" + getCurrentAudio().getId())
                    .build();

            mOkHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                }
            });
        }
    }

    public void addToPlaylist() {
        String owner_id = getCurrentAudio().getOwner_id();
        String audio_id = getCurrentAudio().getId();
        Request request = null;

        if (getCurrentAudio().getOwner_id().equals(mUser_Id)) {
            request = new Request.Builder()
                    .url(BASE_URL + "audio.delete?" + SECOND_PART_URL + mAccessToken
                            + "&audio_id=" + audio_id
                            + "&owner_id=" + owner_id)
                    .build();

            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIaddToPlaylist(REMOVE_FROM_PLAYLIST);
            }
        } else {
            request = new Request.Builder()
                    .url(BASE_URL + "audio.add?" + SECOND_PART_URL + mAccessToken
                            + "&audio_id=" + audio_id
                            + "&owner_id=" + owner_id)
                    .build();

            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIaddToPlaylist(ADD_TO_PLAYLIST);
            }

            mCurrentAudioList.get(mCurrentPosition).setOwner_id(mUser_Id);
        }

        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mServiceCallbacksUpdateList != null) {
                                mServiceCallbacksUpdateList.updateDownloadedList();
                            }
                        }
                    });
                }
            }
        });

        // TODO: 26.12.2016 1 
        /*final VKApiAudio currentAudio = getCurrentAudio();
        String audio_id = String.valueOf(currentAudio.id);
        String owner_id = String.valueOf(currentAudio.owner_id);

        if (currentAudio.owner_id == MainActivity.USER_ID) {
            VKRequest request = VKApi.audio().delete(VKParameters.from("audio_id", audio_id, "owner_id", owner_id));

            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIaddToPlaylist(REMOVE_FROM_PLAYLIST);
            }

            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    nextMusic();
                }

            });
        } else {
            VKRequest request = VKApi.audio().add(VKParameters.from("audio_id", audio_id, "owner_id", owner_id));

            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIaddToPlaylist(ADD_TO_PLAYLIST);
            }

            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    try {
                        currentAudio.owner_id = MainActivity.USER_ID;
                        currentAudio.id = response.json.getInt("response");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }*/
    }

    public Music getCurrentAudio() {
        return mCurrentAudioList.get(mCurrentPosition);
    }

    public List<Music> getPlayList() {
        return mCurrentAudioList;
    }

    public boolean isInMyPlaylist() {
        return getCurrentAudio().getOwner_id().equals(mUser_Id);
    }

    public boolean isBroadcast() {
        return mIsBroadcast;
    }

    public void updateUIMusicImage() {
        if (mMusicImage != null) {
            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIMusicImage(mMusicImage);
            }
        } else {
            mMusicImageTask.cancel(true);
            mMusicImageTask = new MusicImageTask();
            mMusicImageTask.execute(getCurrentAudio());
        }
    }

    public void updateUIDefaultImage() {
        if (mServiceCallbacksMusicController != null) {
            mServiceCallbacksMusicController.updateUIMusicImage(null);
        }
    }

    private void updateUIDownloadMusic(boolean enabled) {
        if (mServiceCallbacksMusicController != null) {
            mServiceCallbacksMusicController.updateUIDownloadMusic(enabled);
        }
    }

    class MusicImageTask extends AsyncTask<Music, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Music... params) {
            Bitmap bitmap = null;
            FFmpegMediaMetadataRetriever mmr = new FFmpegMediaMetadataRetriever();

            try {
                mmr.setDataSource(params[0].getUrl());
            } catch (Exception e) {
                this.cancel(true);
            }

            byte[] data = mmr.getEmbeddedPicture();

            if (data != null) {
                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            }

            mmr.release();
            mmr = null;

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap image) {
            mMusicImage = image;

            if (mServiceCallbacksMusicController != null) {
                mServiceCallbacksMusicController.updateUIMusicImage(mMusicImage);
            }

            if (mServiceCallbacksMainActivity != null) {
                mServiceCallbacksMainActivity.updateMiniControllerImage(mMusicImage);
            }

            if (mServiceCallbacksSearchActivity != null) {
                mServiceCallbacksSearchActivity.updateMiniControllerImage(mMusicImage);
            }

            showNotification();
        }
    }

    public void downloadMusic(Music music) {
        if (music.getUrl().startsWith("file")) {
            Toast.makeText(getApplicationContext(), getString(R.string.already_loaded), Toast.LENGTH_SHORT).show();
        } else {
            String id = music.getId();
            String owner_id = music.getOwner_id();

            //updateUIDownloadMusic(false);

            downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            Uri download_uri = Uri.parse(music.getUrl());
            DownloadManager.Request request = new DownloadManager.Request(download_uri);

            request.setTitle(music.getTitle());
            request.setDescription(music.getArtist());
            //request.setDestinationInExternalPublicDir(LOCAL_STORE_URL, getCurrentMusicArtist() + " - " + getCurrentMusicTitle() + ".mp3");
            request.setDestinationInExternalPublicDir(LOCAL_STORE_URL, String.valueOf(System.currentTimeMillis()) + owner_id + id);
            downloadReference = downloadManager.enqueue(request);

            downloadList.put(downloadReference, music);

            Toast.makeText(getApplicationContext(), getString(R.string.music_download_started), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                switch (mMusicState.lastKnownAudioFocusState) {
                    case MusicState.UNKOWN:
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        if (mMusicState.wasPlayingWhenTransientLoss) {
                            mMediaPlayer.start();
                        }
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                        mMediaPlayer.setVolume(VOLUME_NORMAL, VOLUME_NORMAL);
                        break;
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                mMediaPlayer.pause();
                mMusicState.PLAY = mMediaPlayer.isPlaying() == true ? MusicState.PlayState.PLAY : MusicState.PlayState.PAUSE;
                mMusicState.audioFocusGranted = false;
                mMusicState.lastKnownAudioFocusState = MusicState.UNKOWN;
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                mMusicState.wasPlayingWhenTransientLoss = mMediaPlayer.isPlaying();
                mMediaPlayer.pause();
                mMusicState.audioFocusGranted = false;
                mMusicState.lastKnownAudioFocusState = -2;
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                mMediaPlayer.setVolume(VOLUME_LOWER, VOLUME_LOWER);
                mMusicState.audioFocusGranted = false;
                mMusicState.lastKnownAudioFocusState = -3;
                break;
        }
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            if (downloadList.containsKey(downloadId)) {
                updateUIDownloadMusic(true);

                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(downloadReference);

                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

                Cursor cursor = downloadManager.query(query);

                if (cursor.moveToFirst()) {
                    int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));

                    if (DownloadManager.STATUS_SUCCESSFUL == status) {
                        Music musicData = downloadList.get(downloadId);

                        String local_uri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));

                        /*if (mServiceCallbacksUpdateList != null) {
                            mServiceCallbacksUpdateList.updateDownloadedList();
                        }*/

                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        Music music = realm.createObject(Music.class);
                        music.setTitle(musicData.getTitle());
                        music.setArtist(musicData.getArtist());
                        music.setOwner_id(musicData.getOwner_id());
                        music.setId(musicData.getId());
                        music.setDuration(musicData.getDuration());
                        music.setUrl(local_uri);
                        realm.commitTransaction();
                        realm.close();

                        downloadList.remove(downloadId);
                        Toast.makeText(getApplicationContext(), R.string.download_successfully, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                    }
                }

                cursor.close();
            }
        }
    };

    private class HeadsetReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);

                switch (state) {
                    case 0:
                        if (isPlaying()) {
                            pause();
                        }
                        break;
                }
            }
        }
    }
}
