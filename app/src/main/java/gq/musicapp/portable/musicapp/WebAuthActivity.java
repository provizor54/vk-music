package gq.musicapp.portable.musicapp;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import okhttp3.HttpUrl;

public class WebAuthActivity extends AppCompatActivity {

    private WebView mWebView;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences mSharedPreferences;
    private String mUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_auth);

        mWebView = (WebView) findViewById(R.id.web_view);
        mWebView.getSettings().setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().removeAllCookies(null);
        } else {
            CookieManager.getInstance().removeAllCookie();
        }

        mWebView.clearCache(true);
        mWebView.clearHistory();

        mWebView.setWebViewClient(new WebViewClient() {
            @TargetApi(24)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                String url = request.getUrl().toString();

                if (url.contains("https://oauth.vk.com/blank.html#")) {
                    mWebView.setVisibility(View.INVISIBLE);

                    saveToken(url);
                }

                view.loadUrl(url);

                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("https://oauth.vk.com/blank.html#")) {
                    mWebView.setVisibility(View.INVISIBLE);

                    saveToken(url);
                }

                view.loadUrl(url);

                return true;
            }

            @TargetApi(23)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                view.loadUrl("about:blank");
                showMessage(error.getDescription().toString());
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                view.loadUrl("about:blank");
                showMessage(description);
            }
        });

        mWebView.loadUrl(mUrl);
    }

    public void saveToken(String url) {
        url = url.replace("#", "?");
        HttpUrl httpUrl = HttpUrl.parse(url);

        String accessToken = httpUrl.queryParameter("access_token");
        String userId = httpUrl.queryParameter("user_id");

        mEditor = getSharedPreferences("data_store", MODE_PRIVATE).edit();
        mEditor.putString("access_token", accessToken);
        mEditor.putString("user_id", userId);
        mEditor.apply();

        Intent intent = new Intent(WebAuthActivity.this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    private void showMessage(String errorDescription) {
        new AlertDialog.Builder(WebAuthActivity.this)
                .setMessage(errorDescription)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mWebView.loadUrl(mUrl);
                    }
                }).show();
    }

    /*@Override
    protected void onDestroy() {
        mWebView.clearCache(true);
        mWebView.clearHistory();

        CookieManager.getInstance().removeAllCookies();
        super.onDestroy();
    }*/
}
